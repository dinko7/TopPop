package com.example.toppop.networking.repository

import com.example.toppop.base.BaseRepository
import com.example.toppop.networking.apis.DeezerApi
import com.example.toppop.networking.models.Album
import com.example.toppop.networking.models.Track
import com.example.toppop.utils.errorhandling.NetworkResult

class DeezerRepository(private val service: DeezerApi) : BaseRepository(), IDeezerRepository {

    override suspend fun getTopTracks(): NetworkResult<List<Track>> {
        return callApi { service.getTopTracks().data }
    }

    override suspend fun getAlbum(albumId: Int): NetworkResult<Album> {
        return callApi { service.getAlbum(albumId) }
    }

}