package com.example.toppop.networking.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ListObject<T>(
    val data: List<T>
)