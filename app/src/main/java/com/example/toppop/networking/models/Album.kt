package com.example.toppop.networking.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Album(
    val id: Int,
    val title: String,
    val upc: String?,
    val link: String?,
    val share: String?,
    val cover: String,
    val cover_small: String,
    val cover_medium: String,
    val cover_big: String,
    val cover_xl: String,
    val md5_image: String,
    val genre_id: Int?,
    val genres: Any?,
    val label: String?,
    val nb_tracks: Int?,
    val duration: Int?,
    val fans: Int?,
    val rating: Int?,
    val release_date: Any?,
    val record_type: String?,
    val available: Boolean?,
    val tracklist: String,
    val explicit_lyrics: Boolean?,
    val explicit_content_lyrics: Int?,
    val explicit_content_cover: Int?,
    val contributors: List<Any>?,
    val artist: Artist?,
    val tracks: ListObject<Track>?
)