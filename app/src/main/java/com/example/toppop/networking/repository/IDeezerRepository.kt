package com.example.toppop.networking.repository

import com.example.toppop.networking.models.Album
import com.example.toppop.networking.models.Track
import com.example.toppop.utils.errorhandling.NetworkResult

interface IDeezerRepository {

    suspend fun getTopTracks() : NetworkResult<List<Track>>

    suspend fun getAlbum(albumId: Int) : NetworkResult<Album>
}