package com.example.toppop.networking.apis

import com.example.toppop.networking.models.Album
import com.example.toppop.networking.models.response.ChartResponse
import com.example.toppop.utils.errorhandling.NetworkResult
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

private const val BASE_URL = "https://api.deezer.com/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface DeezerApi {

    @GET("chart/0/tracks")
    suspend fun getTopTracks(): ChartResponse

    @GET("album/{album_id}")
    suspend fun getAlbum(@Path("album_id") albumId: Int): Album
}

object DeezerApiClient {
    val retrofitService : DeezerApi by lazy {
        retrofit.create(DeezerApi::class.java)
    }
}