package com.example.toppop.networking.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Track(
    val id: Int,
    val readable: Boolean?,
    val title: String,
    val title_short: String,
    val title_version: String,
    val unseen: Boolean?,
    val isrc: Boolean?,
    val link: String,
    val share: String?,
    val duration: Int,
    val track_position: Int?,
    val disk_number: Int?,
    val rank: Int,
    val release_date: String?,
    val explicit_lyrics: Boolean,
    val explicit_content_lyrics: Int,
    val explicit_content_cover: Int,
    val preview: String?,
    val bpm: Float?,
    val gain: Float?,
    val available_countries: List<Any>?,
    val alternative: Any?,
    val contributors: List<Any>?,
    val md5_image: String,
    val position: Int?,
    val artist: Artist,
    val album: Album?
)