package com.example.toppop.networking.models.response

import com.example.toppop.networking.models.Track
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChartResponse(
    val data: List<Track>,
    val total: Int
)