package com.example.toppop.base

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.example.toppop.R
import com.example.toppop.utils.manager.DialogManager
import com.google.android.material.snackbar.Snackbar

/**
 * The [BaseFragment] is [Fragment] with common properties shared by fragments in this app.
 * Such common properties are:
 *      a) Error displaying
 *      b) Loading dialog display
 */
abstract class BaseFragment : Fragment() {

    protected val dialogManager by lazy { DialogManager(requireContext()) }

    private val loadingDialog by lazy { dialogManager.getLoadingDialog() }

    open fun showMessage(message: String) {
        Snackbar.make(requireView(), message, Snackbar.LENGTH_SHORT).show();
    }

    open fun showError(message: String?) {
        showMessage(
            if (message.isNullOrBlank())
                getString(R.string.an_unknown_error_occurred)
            else
                getString(R.string.network_error_with_cause, message)
        )
    }

    open fun showLoading() {
        if (!loadingDialog.isShowing)
            loadingDialog.show()
    }

    open fun hideLoading() {
        if (loadingDialog.isShowing)
            loadingDialog.hide()
    }
}