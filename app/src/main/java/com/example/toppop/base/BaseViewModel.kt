package com.example.toppop.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.toppop.utils.errorhandling.NetworkResult
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

/**
 * The [BaseViewModel] is [ViewModel] with common properties shared by viewmodels in this app.
 * It also provides a [CoroutineScope] for performing network requests.
 * Common properties are:
 *      a) Error displaying [_errors]
 *      b) Loading dialog display [_loading]
 *      c) Handling network requests [processNetworkRequest method]
 */
open class BaseViewModel : ViewModel(), CoroutineScope {

    private val _errors = MutableLiveData<String>()
    val errors: LiveData<String>
        get() = _errors

    protected val _loading = MediatorLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val errorHandler = CoroutineExceptionHandler { _, throwable -> Timber.w(throwable) }
    private val vmJob = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.Main + vmJob + errorHandler

    init {
        _loading.addSource(errors) {
            //stop loading animation if an error shows
            _loading.value = false
        }
    }

    protected inline fun <T> processNetworkRequest(result: NetworkResult<T>, onSuccess: (value: T) -> Unit) {
        when (result) {
            is NetworkResult.Success -> {
                result.value.let {
                    _loading.value = false
                    Timber.tag(this::class.java.simpleName).i("Network call successful: $it")
                    onSuccess(it)
                }
            }

            is NetworkResult.Error -> {
                Timber.tag(this::class.java.simpleName).e("Network error.")
                showNetworkError(result.message)
            }
        }
    }

    protected open fun showNetworkError(error: String) {
        _errors.value = error
    }

    protected fun startLoading() {
        _loading.value = true
    }
}