package com.example.toppop.base

import com.example.toppop.utils.errorhandling.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


abstract class BaseRepository {

    protected suspend fun <T> callApi(apiCall: suspend () -> T): NetworkResult<T> {
        return withContext(Dispatchers.IO) {
            try {
                NetworkResult.Success(apiCall())
            } catch (e: Exception) {
                NetworkResult.Error(e.message.orEmpty())
            }
        }
    }
}