package com.example.toppop.utils.errorhandling

sealed class NetworkResult<out T> {
    data class Success<out T>(val value: T): NetworkResult<T>()
    data class Error(val message: String): NetworkResult<Nothing>()
}