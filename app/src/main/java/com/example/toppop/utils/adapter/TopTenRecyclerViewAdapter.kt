package com.example.toppop.utils.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.toppop.R
import com.example.toppop.networking.models.Track
import com.example.toppop.utils.format.formatDuration
import kotlinx.android.synthetic.main.item_track.view.*

typealias LambdaTrackTrackToInt = (Track, Track) -> Int

class TopTenRecyclerViewAdapter(private val tracks: ArrayList<Track>, private val onClickListener: TrackOnClickListener) :
    RecyclerView.Adapter<TopTenRecyclerViewAdapter.ViewHolder>() {

    companion object {
        val sortNormal : LambdaTrackTrackToInt = { track1, track2 ->
            if (track1.position==null || track2.position==null) {
                track1.rank - track2.rank
            } else {
                track1.position - track2.position
            }

        }
        val sortAscending : LambdaTrackTrackToInt = { track1, track2 ->
            track1.duration - track2.duration
        }
        val sortDescending: LambdaTrackTrackToInt = { track1, track2 ->
            track2.duration - track1.duration
        }
    }

    class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val recyclerViewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_track, parent, false)
        return ViewHolder(recyclerViewItem)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.apply {
            text_position.text = (position + 1).toString()
            text_track_name.text = tracks[position].title
            text_track_artist.text = tracks[position].artist.name
            text_track_duration.text = formatDuration(tracks[position].duration)
            setOnClickListener{
                onClickListener.onTrackClicked(tracks[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return tracks.size
    }

    fun updateContent(newTracks: List<Track>) {
        tracks.clear()
        tracks.addAll(newTracks)
        notifyDataSetChanged()
    }

    fun sortContent(comparator: LambdaTrackTrackToInt) {
        tracks.sortWith(comparator)
        notifyDataSetChanged()
    }
}