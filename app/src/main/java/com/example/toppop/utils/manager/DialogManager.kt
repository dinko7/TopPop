package com.example.toppop.utils.manager

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import com.example.toppop.R

class DialogManager(private val context: Context) {

    fun getLoadingDialog() : Dialog {
        return AlertDialog.Builder(context).setView(R.layout.dialog_loading).create().apply {
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }
    }

}