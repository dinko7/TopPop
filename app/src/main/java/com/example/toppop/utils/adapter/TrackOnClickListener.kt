package com.example.toppop.utils.adapter

import com.example.toppop.networking.models.Track

interface TrackOnClickListener {
    fun onTrackClicked(track: Track)
}