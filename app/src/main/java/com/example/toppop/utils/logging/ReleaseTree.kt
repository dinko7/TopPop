package com.example.toppop.utils.logging

import timber.log.Timber

/**
 * ReleaseTree is a Timber tree which is used with release build.
 * It doesn't log anything.
 */
class ReleaseTree : Timber.Tree() {
    override fun log(
        priority: Int,
        tag: String?,
        message: String,
        throwable: Throwable?
    ) {
        //Don't log anything
    }
}
