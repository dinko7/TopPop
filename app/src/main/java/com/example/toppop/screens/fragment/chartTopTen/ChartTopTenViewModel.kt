package com.example.toppop.screens.fragment.chartTopTen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.toppop.base.BaseViewModel
import com.example.toppop.networking.models.Track
import com.example.toppop.networking.apis.DeezerApiClient
import com.example.toppop.networking.repository.DeezerRepository
import kotlinx.coroutines.launch

class ChartTopTenViewModel : BaseViewModel() {

    private val deezerService by lazy { DeezerApiClient.retrofitService }
    private val deezerRepository by lazy { DeezerRepository(deezerService) }

    private val _listTopTen = MutableLiveData<List<Track>>()
    val listTopTen: LiveData<List<Track>>
        get() = _listTopTen


    init {
        getTopTen()
    }

    fun getTopTen() {
        startLoading()
        viewModelScope.launch {
            processNetworkRequest(deezerRepository.getTopTracks()) {
                _listTopTen.value = it
            }
        }
    }

}