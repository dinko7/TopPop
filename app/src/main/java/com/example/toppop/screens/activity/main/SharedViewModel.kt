package com.example.toppop.screens.activity.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.toppop.base.BaseViewModel
import com.example.toppop.networking.models.Track


/**
 * [SharedViewModel] is activity-level viewmodel which is used to pass
 * the selected track from recycler view to the details fragment.
 */
class SharedViewModel : BaseViewModel() {

    private val _selectedTrack = MutableLiveData<Track>()
    val selectedTrack: LiveData<Track>
        get() = _selectedTrack


    fun selectTrack(track: Track) {
        _selectedTrack.value = track
    }
}