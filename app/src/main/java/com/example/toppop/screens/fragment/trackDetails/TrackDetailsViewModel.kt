package com.example.toppop.screens.fragment.trackDetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.toppop.base.BaseViewModel
import com.example.toppop.networking.apis.DeezerApiClient
import com.example.toppop.networking.models.Album
import com.example.toppop.networking.repository.DeezerRepository
import kotlinx.coroutines.launch

class TrackDetailsViewModel : BaseViewModel() {

    private val deezerService by lazy { DeezerApiClient.retrofitService }
    private val deezerRepository by lazy { DeezerRepository(deezerService) }

    private val _album = MutableLiveData<Album>()
    val album: LiveData<Album>
        get() = _album

    fun getAlbum(albumId: Int) {
        startLoading()
        viewModelScope.launch {
            processNetworkRequest(deezerRepository.getAlbum(albumId)) {
                _album.value = it
            }
        }
    }
}