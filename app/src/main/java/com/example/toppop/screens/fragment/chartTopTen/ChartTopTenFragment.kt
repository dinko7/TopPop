package com.example.toppop.screens.fragment.chartTopTen

import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.toppop.R
import com.example.toppop.base.BaseFragment
import com.example.toppop.networking.models.Track
import com.example.toppop.screens.activity.main.SharedViewModel
import com.example.toppop.utils.adapter.TopTenRecyclerViewAdapter
import com.example.toppop.utils.adapter.TopTenRecyclerViewAdapter.Companion.sortAscending
import com.example.toppop.utils.adapter.TopTenRecyclerViewAdapter.Companion.sortDescending
import com.example.toppop.utils.adapter.TopTenRecyclerViewAdapter.Companion.sortNormal
import com.example.toppop.utils.adapter.TrackOnClickListener
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.android.synthetic.main.fragment_chart_top_ten.*

class ChartTopTenFragment : BaseFragment() {

    private val chartTopTenViewModel: ChartTopTenViewModel by viewModels()
    private val sharedViewModel : SharedViewModel by activityViewModels()

    private lateinit var recyclerViewAdapter: TopTenRecyclerViewAdapter
    private lateinit var recyclerViewLayoutManager: LinearLayoutManager

    //to prevent swipe to refresh triggering upon scrolling up
    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            swipe_refresh_layout_chart_top_ten.isEnabled =
                recyclerViewLayoutManager.findFirstCompletelyVisibleItemPosition() == 0
        }
    }

    private val trackOnClickListener = object : TrackOnClickListener {
        override fun onTrackClicked(track: Track) {
            sharedViewModel.selectTrack(track)
            findNavController().navigate(R.id.action_chartTopTenFragment_to_trackDetailsFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chart_top_ten, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        initObservers()
    }

    private fun initUI() {
        setHasOptionsMenu(true) //enable options menu
        initRecyclerView()
        initSwipeToRefresh()
    }

    private fun initSwipeToRefresh() {
        swipe_refresh_layout_chart_top_ten.isNestedScrollingEnabled = false
        swipe_refresh_layout_chart_top_ten.setOnRefreshListener {
            chartTopTenViewModel.getTopTen()
        }
    }

    private fun initRecyclerView() {
        recyclerViewAdapter = TopTenRecyclerViewAdapter(arrayListOf(), trackOnClickListener)
        recyclerViewLayoutManager = LinearLayoutManager(requireContext())

        recycler_view_chart_top_ten.apply {
            setHasFixedSize(true)
            addOnScrollListener(recyclerViewOnScrollListener)
            adapter = recyclerViewAdapter
            layoutManager = recyclerViewLayoutManager
        }
    }

    private fun initObservers() {
        chartTopTenViewModel.errors.observe(viewLifecycleOwner, {
            showError(
                if (it.isNotEmpty())
                    it else getString(R.string.network_error_occurred)
            )
        })

        chartTopTenViewModel.loading.observe(viewLifecycleOwner, {
            if (it)
                showLoading()
            else
                hideLoading()
        })

        chartTopTenViewModel.listTopTen.observe(viewLifecycleOwner, {
            updateUI()
        })
    }

    private fun updateUI() {
        updateRecyclerView()
        stopSwipeRefreshAnimation()
    }

    private fun updateRecyclerView() {
        recyclerViewAdapter.updateContent(chartTopTenViewModel.listTopTen.value!!)
    }

    private fun stopSwipeRefreshAnimation() {
        if (swipe_refresh_layout_chart_top_ten.isRefreshing)
            swipe_refresh_layout_chart_top_ten.isRefreshing = false
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        //inflate menu
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item.itemId.let {
            if (it == R.id.sort_normal) {
                recyclerViewAdapter.sortContent(sortNormal)
            } else if (it == R.id.sort_asc) {
                recyclerViewAdapter.sortContent(sortAscending)
            } else if (it == R.id.sort_desc) {
                recyclerViewAdapter.sortContent(sortDescending)
            }
        }

        return true
    }

}