package com.example.toppop.screens.fragment.trackDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.example.toppop.R
import com.example.toppop.base.BaseFragment
import com.example.toppop.networking.models.Track
import com.example.toppop.screens.activity.main.SharedViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_track_details.*


class TrackDetailsFragment : BaseFragment() {

    private val trackDetailsViewModel : TrackDetailsViewModel by viewModels()
    private val sharedViewModel : SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_track_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
    }

    private fun initObservers() {

        trackDetailsViewModel.errors.observe(viewLifecycleOwner, {
            showError(
                if (it.isNotEmpty())
                    it else getString(R.string.network_error_occurred)
            )
        })

        trackDetailsViewModel.loading.observe(viewLifecycleOwner, {
            if (it)
                showLoading()
            else
                hideLoading()
        })

        sharedViewModel.selectedTrack.observe(viewLifecycleOwner, {
            updateSongDetails()
            it.album?.id?.let {id ->
                trackDetailsViewModel.getAlbum(id)
            }
        })

        trackDetailsViewModel.album.observe(viewLifecycleOwner, {
            updateAlbumDetails()
        })

    }

    private fun updateAlbumDetails() {
        trackDetailsViewModel.album.value?.let {
            text_album_name.text = it.title
            loadAlbumImage(it.cover_big)
        }
        trackDetailsViewModel.album.value?.tracks?.let {
            text_album_songs.text = getTrackListAsString(it.data)
        }

    }

    private fun updateSongDetails() {
        sharedViewModel.selectedTrack.value?.let {
            text_song_name.text = it.title
            text_artist_name.text = it.artist.name
        }
    }

    private fun loadAlbumImage(imageUrl: String) {
        Picasso.get().load(imageUrl).into(img_album)
    }

    private fun getTrackListAsString(tracks: List<Track>) :String {
        val sb = StringBuilder()
        for (i in 0 until tracks.size-1) {
            tracks[i].let {
                if (it.title != sharedViewModel.selectedTrack.value?.title) {
                    sb.append(it.title).append(getString(R.string.track_separator))
                }
            }
        }
        sb.append(tracks.last().title)

        return sb.toString()
    }
}